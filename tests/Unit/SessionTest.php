<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Session;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $session = new Session();
        $session->name = "Test Session";
        $this->assertTrue($session->save());
        $this->assertNotEmpty($session->uuid);
    }
}
