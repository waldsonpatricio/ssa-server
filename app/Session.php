<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviors\UsesUuid;

class Session extends Model
{
    use UsesUuid;

    protected $guarded = [];

    public function spans()
    {
        return $this->hasMany(Span::class);
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function toArray()
    {
        $response = parent::toArray();
        $response['config']   = url()->to(\Storage::url($response['config']));
        $response['stations'] = url()->to(\Storage::url($response['stations']));

        return $response;
    }
}
