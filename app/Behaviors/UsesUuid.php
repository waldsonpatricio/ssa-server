<?php
namespace App\Behaviors;

use Ramsey\Uuid\Uuid as BaseUuid;

trait UsesUuid
{
    public static function bootUsesUuid()
    {
        static::creating(function ($model) {
            $model->uuid = BaseUuid::uuid4();
        });
    }
}
