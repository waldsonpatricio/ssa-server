<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Span;
use App\Services\SpanProcessor;

class ProcessSpan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $span = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Span $span)
    {
        $this->span = $span;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SpanProcessor $processor)
    {
        $processor->processSpan($this->span);
    }
}
