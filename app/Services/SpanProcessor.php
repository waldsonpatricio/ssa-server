<?php
namespace App\Services;

use App\Span;
use Symfony\Component\Process\Process;

class SpanProcessor
{

    const INPUT_DIR  = 'input';
    const OUTPUT_DIR = 'output';

    public function processSpan(Span $span)
    {

        try {
            $this->prepare($span);
            $this->run($span);
            $this->moveResultToPublicFolder($span);
            $span->status = Span::STATUS_DONE;
            $span->save();
        } catch (\Exception $ex) {
            $span->status = Span::STATUS_FAILED;
            $span->save();
            throw $ex;
        }
    }

    private function getRunDirectory(Span $span)
    {
        return storage_path('run-' . $span->uuid);
    }

    private function getInputDir(Span $span)
    {
        return $this->getRunDirectory($span) . '/' . self::INPUT_DIR;
    }

    private function getOutputDir(Span $span)
    {
        return $this->getRunDirectory($span) . '/' . self::OUTPUT_DIR;
    }

    private function prepare(Span $span)
    {
        $session = $span->session;
        $logs    = $span->logs;

        $runDir    = $this->getRunDirectory($span);
        $inputDir  = $this->getInputDir($span);
        $outputDir = $this->getOutputDir($span);

        $dirs = [$runDir, $inputDir, $outputDir];

        foreach ($dirs as $dir) {
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
                \Log::debug("Creating folder: " . $dir);
            }
        }

        symlink(storage_path('app/public/' . $session->stations), $inputDir . '/station.dat');

        $jsonFile = json_decode(file_get_contents(storage_path('app/public/' . $session->config)));
        $this->overrideConfigOptions($jsonFile);
        $this->saveJsonFile($jsonFile, $runDir . '/ssa.json');

        foreach ($logs as $log) {
            \Log::debug("Creating folder: " . $dir);
            symlink(storage_path('app/public/' . $log->log), $inputDir . '/' . $log->station . '.dat');
        }
    }

    private function saveJsonFile($jsonObject, $path)
    {
        $handle = fopen($path, 'w');
        fwrite($handle, json_encode($jsonObject));
        fclose($handle);
    }

    private function overrideConfigOptions($configObject)
    {
        //TODO put these values on .env file
        $configObject->sequential           = false;
        $configObject->stationsFileTemplate = '{name}.dat';
        $configObject->maxFileSaveThreads   = 4;
        $configObject->parallelOts          = 5000;
        $configObject->inputDir             = self::INPUT_DIR;
        $configObject->outputDir            = self::OUTPUT_DIR;
        $configObject->firstOt              = 0;
        $configObject->maxOts               = null;
    }

    private function run(Span $span)
    {
        $span->status = Span::STATUS_RUNNING;
        $span->save();
        $pssa = new Process([config('pssa.path')], $this->getRunDirectory($span));
        $pssa->mustRun();
    }

    private function moveResultToPublicFolder(Span $span)
    {
        $outputDir    = $this->getOutputDir($span);
        $newOutputDir = storage_path('app/public/result-' . $span->uuid);
        symlink($outputDir, $newOutputDir);
        $span->result = 'result-' . $span->uuid . '/output.info';
        $span->save();
    }
}
