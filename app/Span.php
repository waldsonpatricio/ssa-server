<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviors\UsesUuid;

class Span extends Model
{
    protected $fillable = ['session_id', 'status', 'result'];

    use UsesUuid;

    const STATUS_WAITING_LOGS = 'waiting_logs';
    const STATUS_ON_QUEUE     = 'on_queue';
    const STATUS_RUNNING      = 'running';
    const STATUS_DONE         = 'done';
    const STATUS_FAILED       = 'failed';

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function toArray()
    {
        $response = parent::toArray();
        if (!empty($response['result'])) {
            $response['result']   = url()->to(\Storage::url($response['result']));
        }

        return $response;
    }
}
