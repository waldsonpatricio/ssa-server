<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviors\UsesUuid;

class Log extends Model
{
    use UsesUuid;

    protected $fillable = ['log', 'span_id', 'station'];

    public function span()
    {
        return $this->belongsTo(Span::class);
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function toArray()
    {
        $response = parent::toArray();
        $response['log']   = url()->to(\Storage::url($response['log']));

        return $response;
    }
}
