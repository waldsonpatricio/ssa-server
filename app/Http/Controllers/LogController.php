<?php

namespace App\Http\Controllers;

use App\Log;
use App\Span;
use App\Session;
use Illuminate\Http\Request;
use App\Jobs\ProcessSpan;

class LogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Session $session, Span $span)
    {
        return Log::where('span_id', $span->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Session $session, Span $span, Request $request)
    {
        $keys     = array_keys($request->all());
        $stations = [];

        foreach ($keys as $key) {
            if (!preg_match('#^station_(.+)$#', $key, $matches)) {
                continue;
            }
            $stations[$key] = $matches[1];
        }

        if (empty($stations)) {
            return response()->json(['message' => 'You must send log files.'], 400);
        }


        $response      = [];
        $hasAnySuccess = false;

        foreach ($stations as $fileKey => $stationName) {
            if (!$request->hasFile($fileKey)) {
                $response[$fileKey] = ['message' => 'Must be a file.'];
                continue;
            }
            $logFile = $request->file($fileKey);

            if (!$logFile->isValid()) {
                $response[$fileKey] = ['message' => 'Invalid upload.'];
                continue;
            }

            $log = Log::firstOrCreate(['span_id' => $span->id, 'station' => $stationName]);
            $log->log = $logFile->storeAs('logs-' . $span->uuid, $log->uuid, 'public');
            $log->save();

            $response[$fileKey] = ['message' => 'OK', 'log' => $log];
            $hasAnySuccess = true;
        }
        //check if all logs of this span has arrived
        //if so, put span on queue to be processed
        $span->fresh();
        if ($span->logs->count() >= $span->session->stations_qtty) {
            $span->status = Span::STATUS_ON_QUEUE;
            $span->save();
            ProcessSpan::dispatch($span);
        }




        return response()->json(
            $response,
            $hasAnySuccess ? 200 : 400
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Log  $span
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session, Span $span, Log $log)
    {
        return $log;
    }
}
