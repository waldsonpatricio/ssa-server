<?php

namespace App\Http\Controllers;

use App\Span;
use App\Session;
use Illuminate\Http\Request;

class SpanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Session $session)
    {
        return Span::where('session_id', $session->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Session $session)
    {
        $span = Span::create(['session_id' => $session->id, 'status' => Span::STATUS_WAITING_LOGS]);
        return response()->json(['message' => 'OK', 'span' => $span]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Span  $span
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session, Span $span)
    {
        //
        return Span::where('session_id', $session->id)->where('id', $span->id)->firstOrFail();
    }
}
