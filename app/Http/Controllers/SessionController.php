<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Session::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'required',
            'config'   => 'required',
            'stations' => 'required'
        ]);

        if (!$request->hasFile('config')) {
            return response()->json(['message' => 'Config file must be provided.'], 400);
        }

        $config = $request->file('config');
        if (!$config->isValid()) {
            return response()->json(['message' => 'Config file upload error.'], 400);
        }

        if (!$request->hasFile('stations')) {
            return response()->json(['message' => 'Stations file must be provided.'], 400);
        }

        $stations = $request->file('stations');
        if (!$stations->isValid()) {
            return response()->json(['message' => 'Stations file upload error.'], 400);
        }

        $session = new Session();
        $session->name = $request->input('name');
        if (!$session->save()) {
            return response()->json(['message' => 'An error ocurred trying to create a new session.'], 400);
        }

        $session->stations = $stations->storeAs('stations', $session->uuid, 'public');
        $session->config   = $config->storeAs('configs', $session->uuid, 'public');

        $session->stations_qtty = $this->calculateNumberOfStations(
            storage_path('app/public/stations/' . $session->uuid)
        );

        if (!$session->save()) {
            return response()->json(['message' => 'An error ocurred trying to create a new session.'], 400);
        }

        return response()->json(['message' => 'OK', 'session' => $session]);
    }

    private function calculateNumberOfStations($filePath)
    {
        //2 lines of header info on station.dat file
        return $this->countLines($filePath) - 2;
    }

    private function countLines($filePath)
    {
        $handle = fopen($filePath, 'r');
        $count  = 0;

        while (!feof($handle)) {
            fgets($handle);
            $count++;
        }

        return $count;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        return $session;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        //
    }
}
