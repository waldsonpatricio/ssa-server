# Dependencies

- PHP 7.1
- Composer
- SQLite (or any other database that Laravel supports)
- PSSA executable installed on the server
- Supervisor

# Installing and running

- composer install
- ./artisan queue:table
- ./artisan migrate
- ./artisan storage:link
- ./artisan serve
- Put the following command to run on Supervisor: ```php /path-to-server/artisan queue:work --tries=5```
    - Or keep it running on a terminal

# Endpoints

```POST /api/sessions``` - Create New Session
```GET /api/sessions``` - View All Sessions
```GET /api/sessions/{uuid}``` - View Session Info


# Samples .env file

```
APP_NAME="PSSA Server"
APP_ENV=local
APP_KEY=base64:ImN17dM49IzCf9zr9upppIhRdjF2QcGfSt380R6Ymws=
APP_DEBUG=false
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=sqlite

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=database
SESSION_DRIVER=file
SESSION_LIFETIME=120
```

# Caveats

- If you want to send big files or multiples files per request, it's
  recommended to increase the values of ```upload_max_filesize``` and
  ```max_file_uploads``` variables of your PHP configuration.
