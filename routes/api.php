<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::get('sessions', 'SessionController@index'); */
Route::post('sessions', 'SessionController@store');
Route::get('sessions/{session}', 'SessionController@show');

Route::get('sessions/{session}/spans', 'SpanController@index');
Route::post('sessions/{session}/spans', 'SpanController@store');
Route::get('sessions/{session}/spans/{span}', 'SpanController@show');

Route::get('sessions/{session}/spans/{span}/logs', 'LogController@index');
Route::post('sessions/{session}/spans/{span}/logs', 'LogController@store');
Route::get('sessions/{session}/spans/{span}/logs/{log}', 'LogController@show');
